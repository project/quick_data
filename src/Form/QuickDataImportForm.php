<?php

namespace Drupal\quick_data\Form;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\TestTools\Extension\SchemaInspector;
use Drupal\quick_data\Batch\CsvImportBatch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class DefaultForm.
 */
class QuickDataImportForm extends FormBase {

  /**
   * Constructs a \Drupal\demo\Form\Multistep\MultistepFormBase.
   *
   * @var int
   */

  protected $step = 1;

  /**
   * Separator.
   *
   * @var string
   */
  protected $separate = " ; ";

  /**
   * Type Exception.
   *
   * @var string[]
   */
  protected $fieldTypeException = [
    'comment',
    'feeds_item',
    'tablefield',
    'entity_reference_revisions',
  ];

  /**
   * Name Exception.
   *
   * @var string[]
   */
  protected $fieldNameException = [
    'type',
    'langcode',
    'revision_id',
    'revision_uid',
    'revision_log',
    'revision_timestamp',
    'default_langcode',
    'revision_default',
    'entity_reference_revisions',
    'revision_translation_affected',
  ];

  /**
   * Constructor import quick data.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entityDisplayRepository
   *   The entity display repository.
   * @param \Symfony\Component\HttpFoundation\Session\SessionInterface $session
   *   Session service.
   */
  public function __construct(protected EntityTypeManagerInterface $entityTypeManager, protected ModuleHandlerInterface $moduleHandler, protected EntityFieldManagerInterface $entityFieldManager, protected EntityDisplayRepositoryInterface $entityDisplayRepository, protected SessionInterface $session) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository'),
      $container->get('session'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $type = '', $bundle = '', $field = '', $entity_id = '') {
    $form_state->set('type', $type);
    $form_state->set('bundle', $bundle);
    $form_state->set('field', $field);
    $form_state->set('entity_id', $entity_id);

    if ($this->step != 1) {
      return $this->quickDataImportProcessCsv($form, $form_state);
    }
    return $this->quickDataImportLoadCsv($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'quick_data_import_form';
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    if (!empty($values["csv_file_upload"])) {
      $fid = reset($values["csv_file_upload"]);
      $this->session->set('csv_file_upload_fid', $fid);
      $form_state->set('csv_file_upload', $form_state->getValue('csv_file_upload'));
      $form_state->set('csv_delimiter', $form_state->getValue('csv_delimiter'));
      $form_state->setRebuild();
      $this->step = 2;
    }
    if (!empty($values["import"])) {
      $store = $form_state->getStorage();
      $file = $this->entityTypeManager->getStorage('file')
        ->load(current($store['csv_file_upload']));
      $filesize = $file->getSize();
      // Big file we will need batch.
      $limitFileSize = 20000;
      if ($filesize > $limitFileSize) {
        if (!isset($store['results'])) {
          $store['results']['rows_imported'] = 0;
        }
        $batch = [
          'title' => $this->t('Importing CSV ...'),
          'operations' => $this->operationsBatchLine($file, $store, $form_state->getUserInput()['col']),
          'init_message' => $this->t('Beginning'),
          'progress_message' => $this->t('Processed @current out of @total.'),
          'error_message' => $this->t('An error occurred during processing'),
          'finished' => '\Drupal\quick_data\Batch\CsvImportBatch::csvImportFinished',
        ];
        batch_set($batch);
      }
      else {
        // If not big file import all.
        $data = $this->convertCsvArray($file, $store['csv_delimiter']);
        array_shift($data);
        $num_imported = CsvImportBatch::quickDataImportMapping($store, $form_state->getUserInput()['col'], $data);
        if ($num_imported) {
          $this->messenger()
            ->addStatus($this->t('@count imported items total.', ['@count' => $num_imported]));
        }
      }
      // Redirection! This is because  $form_state['rebuild'] gets set to TRUE.
      if (!empty($dest_url = $this->getRequest()->query->get('destination'))) {
        $parse = parse_url($dest_url);
        if (!empty($parse['host'])) {
          $response = new TrustedRedirectResponse(Url::fromUri($dest_url)
            ->toString());
          $metadata = $response->getCacheableMetadata();
          $metadata->setCacheMaxAge(0);
          $form_state->setResponse($response);
        }
        else {
          $url = Url::fromUri('internal:/' . $dest_url);
        }
      }
      elseif (!empty($store['entity_id'])) {
        $dest_url = implode('/', [$store['bundle'], $store['entity_id']]);
        $url = Url::fromUri('internal:/' . $dest_url);
      }
      if (!empty($url)) {
        $form_state->setRedirectUrl($url);
      }
    }

  }

  /**
   * {@inheritDoc}
   */
  public function quickDataImportLoadCsv($form, &$form_state) {
    $type = $form_state->get('type');
    $bundle = $form_state->get('bundle');
    $field = $form_state->get('field');
    $entity_id = $form_state->get('entity_id');
    // Page one load csv.
    $entity_types = $this->entityTypeManager->getDefinitions();
    $fieldable_entity_types = array_filter($entity_types, function ($entity_type) {
      return method_exists($entity_type->getOriginalClass(), 'hasField');
    });
    if (!in_array($type, array_keys($fieldable_entity_types))) {
      $this->messenger()->addError($this->t("Your type %type is not available", ['%type' => $bundle]));
      return $form;
    }
    // Check bundle validate.
    $filename = implode('-', [$type, $bundle]) . '.csv';
    if (in_array($type, ['field_collection_item', 'paragraph'])) {
      if (empty($field) || empty($bundle) || empty($entity_id)) {
        $this->messenger()->addError($this->t("Yours variables is not available"));
        return $form;
      }
      $filename = implode('-', [$bundle, $field]) . '.csv';
    }

    $fields_info = $this->fieldsInfo($bundle, $type, $field, $entity_id);
    $options_fields = $this->optionsLoadField($fields_info);
    $validators = [
      'FileExtension' => ['extensions' => 'csv'],
    ];
    $fid = $this->session->get('csv_file_upload_fid', NULL);
    $form['files']['csv_file_upload'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Choose a file'),
      '#description' => '<a class="btn btn-success button button--small" id="export" data-filename="' . $filename . '" download="' . $filename . '"> ' . "Download CSV template" . ' <i class="bi bi-filetype-csv"></i> : <br/></a>
<span id="export-header" >' . implode(',', $options_fields) . '</span>
<br/><i>' . $this->t('Use "%separate" for separate multiple values', ['%separate' => $this->separate]) . '</i>
',
      '#required' => TRUE,
      '#upload_validators' => $validators,
      '#progress_indicator' => 'bar',
      '#progress_message' => $this->t('Please wait...'),
      '#default_value' => $fid ? [$fid] : [],
    ];
    $form['files']['csv_delimiter'] = [
      '#type' => 'select',
      '#title' => $this->t('Character used to separate'),
      '#options' => [
        ',' => ',',
        ';' => ';',
        'tab' => 'Tab',
      ],
      '#empty_option' => $this->t('- Auto -'),
    ];
    $form['files']['field_unique'] = [
      '#type' => 'select',
      '#title' => $this->t('Field unique'),
      '#options' => $options_fields,
      '#empty_option' => $this->t('- None -'),
      '#description' => $this->t('Checks if the value already exists, it will then not be created but updated'),
    ];

    $form['#attached'] = [
      'library' => ['quick_data/drupal.quick_data'],
      'drupalSettings' => [
        'quick_data' => [
          'entity_type' => $form_state->getValue('type'),
        ],
      ],
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Next'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  protected function quickDataImportProcessCsv($form, &$form_state) {
    $type = $form_state->get('type');
    $bundle = $form_state->get('bundle');
    $field = $form_state->get('field');
    $entity_id = $form_state->get('entity_id');
    $fields_info = $this->fieldsInfo($bundle, $type, $field, $entity_id);
    $form_state->set('fields_info', $fields_info);
    $options_fields = ['_none' => $this->t('- Select -')] + $this->optionsLoadField($fields_info);
    $field = [
      '#type' => 'select',
      '#options' => $options_fields,
      '#empty_option' => $this->t('- None -'),
      '#empty_value' => '_none',
    ];
    $flip_fields = !empty($options_fields) ? array_flip($options_fields) : [];
    $csv_delimiter = $form_state->getValue('csv_delimiter');
    $csv_rows = $this->convertCsvArray($form_state->getValue('csv_file_upload'), $csv_delimiter, 10);
    $form_state->setValue('csv_delimiter', $csv_delimiter);
    $storage = $form_state->getStorage();
    $storage['csv_delimiter'] = $csv_delimiter;
    $storage['field_unique'] = $form_state->getValue('field_unique');
    $form_state->setStorage($storage);
    $header = [];
    $header_title = array_shift($csv_rows);
    foreach ($header_title as $col => $title_col) {
      $title_col = trim($title_col);
      $header[$col]["data"] = $field;
      $header[$col]["data"]["#title"] = $title_col;
      $header[$col]["data"]["#name"] = 'col[' . $col . ']';
      if (!empty($flip_fields[$title_col])) {
        $header[$col]["data"]["#value"] = $flip_fields[$title_col];
      }
    }

    $form['table'] = [
      '#theme' => 'table',
      "#title" => $this->t("Mapping field with data"),
      '#header' => $header,
      '#rows' => $csv_rows,
      '#empty' => $this->t('Empty'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['import'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function convertCsvArray($file, &$delimiter = ';', $limit = FALSE) {
    if (is_array($file)) {
      $file = end($file);
    }
    if (is_numeric($file)) {
      $file = $this->entityTypeManager->getStorage('file')->load($file);
    }
    $filename = $file->get('uri')->value;
    $csv_rows = [];
    if (($f = fopen($filename, "r")) !== FALSE) {
      $i = 0;
      if (empty($delimiter)) {
        $firstLine = fgets($f);
        rewind($f);
        foreach ([',', ';', "\t"] as $delimiter) {
          if (count(explode($delimiter, $firstLine)) > 1) {
            break;
          }
        }
      }
      elseif ($delimiter == 'tab') {
        $delimiter = "\t";
      }
      while (($data = fgetcsv($f, 0, $delimiter)) !== FALSE) {
        $csv_rows[] = $data;
        if ($limit && $i++ > $limit) {
          break;
        }
      }
      fclose($f);
    }
    return $csv_rows;
  }

  /**
   * {@inheritDoc}
   */
  private function tableInfo($table) {
    $specification = SchemaInspector::getTablesSpecification($this->moduleHandler, 'quick_data');
    $schema = $specification[$table];
    $infos = [];
    $i = 0;
    foreach ($schema['fields'] as $name => $field) {
      $infos[$name] = [
        'field_name' => $name,
        'label' => !empty($field['description']) ? $field['description'] : $name,
        'type' => $field["type"],
        'columns' => ['value' => ['type' => $field["type"]]],
        'cardinality' => 1,
        'weight' => $i++,
      ];
    }
    return $infos;
  }

  /**
   * {@inheritDoc}
   */
  protected function getTarget($field_definition) {
    $propertyDefinitions = $field_definition->getPropertyDefinitions();
    $type = $field_definition->getType();
    $targetDefinition = [
      'text_with_summary' => ['value', 'summary'],
      'link' => ['uri', 'title'],
      'address' => [
        'organization',
        'given_name',
        'family_name',
        'country_code',
        'address_line1',
        'address_line2',
        'address_line3',
        'postal_code',
        'locality',
        // 'langcode',
        // 'administrative_area',
        // 'dependent_locality',
        // 'additional_name',
        // 'sorting_code',
      ],
      'name' => [
        'title',
        'given',
        'middle',
        'family',
        // 'generational',
        // 'credentials',
      ],
      'daterange' => ['value', 'end_value'],
      'entity_reference' => ['target_id'],
      'image' => ['target_id', 'alt', 'title'],
      'double_field' => ['first', 'second'],
      'triples_field' => ['first', 'second', 'third'],
    ];
    $target = [];
    if ($type == 'data_field') {
      foreach ($propertyDefinitions as $column => $proDef) {
        $target[$column] = (string) $proDef->getLabel();
      }
    }
    if (!empty($targetDefinition[$type])) {
      foreach ($targetDefinition[$type] as $column) {
        if (!empty($propertyDefinitions[$column])) {
          $target[$column] = (string) $propertyDefinitions[$column]->getLabel();
        }
      }
    }
    return $target;
  }

  /**
   * {@inheritDoc}
   */
  private function fieldsInfo($bundle, $type, $field = '', $entity_id = FALSE) {
    if ($bundle == 'data') {
      return $this->tableInfo($type);
    }
    if ($type == 'paragraph') {
      $content_type = $this->entityTypeManager->getStorage($bundle)->load($entity_id)->getType();
      $field_instance = $this->entityTypeManager->getStorage('field_config')
        ->load(implode('.', [$bundle, $content_type, $field]));
      $targetBundle = array_key_first($field_instance->getSetting("handler_settings")["target_bundles"]);
      $field_definitions = $this->entityTypeManager->getStorage($type)->create(['type' => $targetBundle])->getFieldDefinitions();
    }
    else {
      $field_definitions = $this->entityFieldManager->getFieldDefinitions($type, $bundle);
    }

    foreach ($field_definitions as $name => $definition) {
      if (in_array($name, $this->fieldNameException)) {
        continue;
      }
      $fields[$name] = $definition;
    }
    $infos = [];
    $formDisplay = $this->entityDisplayRepository->getFormDisplay($type, $bundle, 'default');
    if (!empty($fields)) {
      foreach ($fields as $field) {
        $field_type = $field->getType();
        $field_info = $field->getFieldStorageDefinition();
        $module = method_exists($field_info, 'get') ? $field_info->get('module') : '';
        if (in_array($field_type, $this->fieldTypeException)) {
          continue;
        }
        $name = $field->getName();
        $fieldComponent = $formDisplay->getComponent($name);
        $infos[$name] = [
          'field_name' => $name,
          'label' => $field->getLabel(),
          'type' => $field->getType(),
          'widget' => $field_info->getType(),
          'columns' => $this->getTarget($field_info),
          'cardinality' => $field_info->getCardinality(),
          'allowed_values' => options_allowed_values($field_info),
          'weight' => $fieldComponent["weight"] ?? 0,
          'module' => $module,
          'schema' => $field_info->getSchema()['columns'],
          'settings' => $field->getSettings(),
        ];
      }
    }
    switch ($type) {
      case 'commerce_store':
        $field_address = $field_definitions["address"];
        $field_info_address = $field_address->getFieldStorageDefinition();
        $infos['address'] = [
          'field_name' => 'address',
          'label' => $field_address->getLabel(),
          'type' => $field_address->getType(),
          'widget' => $field_info_address->getType(),
          'cardinality' => 1,
          'allowed_values' => options_allowed_values($field_info_address),
          'weight' => $fieldComponent["weight"] ?? 0,
          'module' => $field_info_address->get('module'),
          'schema' => $field_info_address->getSchema()['columns'],
          'settings' => $field_address->getSettings(),
          'columns' => $this->getTarget($field_info_address),
        ];
        break;

      case 'commerce_product':
        $field_variations = $field_definitions["variations"];
        $field_info_variations = $field_variations->getFieldStorageDefinition();
        $infos['variations'] = [
          'field_name' => 'variations',
          'label' => $field_variations->getLabel(),
          'type' => $field_variations->getType(),
          'widget' => $field_info_variations->getType(),
          'module' => $bundle,
          'cardinality' => 1,
          'weight' => 1,
        ];
        break;

    }

    return $infos;
  }

  /**
   * {@inheritDoc}
   */
  private function optionsLoadField($infos) {
    uasort($infos, 'Drupal\Component\Utility\SortArray::sortByWeightElement');
    $options = [];
    foreach ($infos as $field_name => $info) {
      if (in_array($info['type'], $this->fieldTypeException)) {
        continue;
      }
      if (!empty($info['columns'])) {
        foreach ($info['columns'] as $name => $label) {
          $options["$field_name:$name"] = trim((string) $info['label'] . ': ' . strip_tags($label));
        }
        continue;
      }
      $options[$field_name] = (string) $info['label'];
    }
    return $options;
  }

  /**
   * Operation Batch import.
   *
   * {@inheritdoc}
   */
  private function operationsBatchLine($file, $store, $mapping) {
    $filename = $file->get('uri')->value;
    $batch = [];
    if ($f = fopen($filename, "r")) {
      $row_index = 0;
      if ($store['csv_delimiter'] == 'tab') {
        $store['csv_delimiter'] = "\t";
      }
      while (($line = fgetcsv($f, 0, $store['csv_delimiter'])) !== FALSE) {
        // Remove first line header csv.
        if (!$row_index++) {
          continue;
        }
        $batch[] = [
          '\Drupal\quick_data\Batch\CsvImportBatch::csvImportLine',
          [[$line], $store, $mapping],
        ];
      }
      fclose($f);
    }
    return $batch;
  }

}
