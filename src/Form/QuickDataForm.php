<?php

namespace Drupal\quick_data\Form;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Component\Transliteration\PhpTransliteration;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\Url;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Drupal\data\Table;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\node\Entity\NodeType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DefaultForm.
 */
class QuickDataForm extends FormBase {

  /**
   * Step for multistep form.
   *
   * @var int
   */

  protected $step = 1;

  /**
   * Temp store.
   *
   * @var \Drupal\user\PrivateTempStore
   */
  protected $store;

  /**
   * Constructor quick data field.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $tempStoreFactory
   *   Temp store.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The entity field manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $fieldTypePluginDefinition
   *   Field type plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $languageManager
   *   The language manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Entity\EntityTypeBundleInfoInterface $entityTypeBundleInfo
   *   The entity type bundle info.
   * @param \Drupal\Component\Plugin\PluginManagerBase $pluginManager
   *   The widget or formatter plugin manager.
   * @param \Drupal\Core\Utility\LinkGeneratorInterface $linkGenerator
   *   The link generator.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Render service.
   */
  public function __construct(
    protected PrivateTempStoreFactory $tempStoreFactory,
    protected EntityFieldManagerInterface $entityFieldManager,
    protected FieldTypePluginManagerInterface $fieldTypePluginDefinition,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected LanguageManagerInterface $languageManager,
    protected ModuleHandlerInterface $moduleHandler,
    protected Connection $database,
    protected EntityTypeBundleInfoInterface $entityTypeBundleInfo,
    protected PluginManagerBase $pluginManager,
    protected LinkGeneratorInterface $linkGenerator,
    protected ModuleExtensionList $moduleExtensionList,
    protected RendererInterface $renderer,
  ) {
    $this->store = $tempStoreFactory->get('quick_data');
    $this->configFactory = $this->configFactory();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new self(
      $container->get('tempstore.private'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('module_handler'),
      $container->get('database'),
      $container->get('entity_type.bundle.info'),
      $container->get('plugin.manager.field.widget'),
      $container->get('link_generator'),
      $container->get('extension.list.module'),
      $container->get('renderer'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quick_data_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Multistep form.
    if ($this->step == 1) {
      $this->step1Form($form, $form_state);
    }
    else {
      // Second form, ask for the database field names.
      $this->step2Form($form, $form_state);
    }

    if ($this->step < 2) {
      $button_label = $this->t('Next');
    }
    else {
      $button_label = $this->t('Finish');
    }
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $button_label,
      '#button_type' => 'primary',
      '#weight' => 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if ($this->step < 2) {
      if (!empty($form_state->getValues())) {
        $this->store->set('valueStep1', $form_state->getValues());
      }
      elseif (!empty($form_state->getUserInput())) {
        $this->store->set('valueStep1', $form_state->getUserInput());
      }
      $form_state->setRebuild();
      $this->step++;
    }
    else {
      $valueStep1 = $this->store->get('valueStep1');
      $this->quickDataCreateFieldDataSubmit($form, $form_state, $valueStep1);
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function step1Form(array &$form, FormStateInterface $form_state) {
    $valueStep1 = $this->store->get('valueStep1');
    $entity_types = $this->entityTypeManager->getDefinitions();
    $fieldable_entity_types = array_filter($entity_types, function ($entity_type) {
      return method_exists($entity_type->getOriginalClass(), 'hasField');
    });
    $entityTypeNotReadable = [
      'file',
      'menu_link_content',
      'path_alias',
      'shortcut',
    ];
    $option = [];
    foreach ($fieldable_entity_types as $type => $entity_type) {
      if (!in_array($type, $entityTypeNotReadable)) {
        $option[$type] = $entity_type->getLabel();
      }
    }
    $type = $form_state->getValue('type');
    if (empty($type) && !empty($valueStep1['type'])) {
      $type = $valueStep1['type'];
    }
    if (empty($type)) {
      $input = $form_state->getUserInput('type');
      $type = $input['type'] ?? NULL;
    }
    // Build a wrapper for the ajax response.
    $form['collect_field_label'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $this->getFormId(),
      ],
    ];
    $form['collect_field_label']['type'] = [
      '#type' => 'select',
      '#title' => $this->t('Type'),
      '#description' => $this->t('Set your type'),
      '#default_value' => $type,
      '#options' => $option,
      '#weight' => 0,
      '#ajax' => [
        'callback' => [$this, 'ajaxQuickDataCollectFieldCallback'],
        'wrapper' => $this->getFormId(),
        'event' => 'change',
        'options' => ['query' => ['ajax_form' => 1]],
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying entry...'),
        ],
      ],
    ];
    $bundle_description = $option[$type] ?? NULL;
    $bundle = $form_state->getValue('bundle');
    if (empty($bundle) && !empty($form_state->getUserInput('bundle'))) {
      $bundle = $form_state->getUserInput('bundle');
      if (!empty($bundle["type"])) {
        $bundle = $bundle["type"];
      }
    }
    if (!empty($valueStep1['bundle'])) {
      $bundle = $valueStep1['bundle'];
    }
    $option_bundle = $this->collectFieldLabelType($type);
    if ($option_bundle) {
      $form['collect_field_label']['bundle'] = [
        '#type' => 'select',
        '#title' => $this->t('Bundle'),
        '#description' => $this->t("Set your %bundle", ['%bundle' => $bundle_description]),
        '#empty_option' => $this->t("- New %bundle -", ['%bundle' => $bundle_description]),
        '#empty_value' => '_none',
        '#options' => $option_bundle,
        '#default_value' => $bundle,
        '#ajax' => [
          'callback' => [$this, 'ajaxQuickDataCollectFieldCallback'],
          'wrapper' => $this->getFormId(),
          'event' => 'change',
          'options' => ['query' => ['ajax_form' => 1]],
          'progress' => [
            'type' => 'throbber',
            'message' => $this->t('Verifying entry...'),
          ],
        ],
      ];
    }

    $name = $form_state->getValue('name');
    if (!empty($valueStep1['name'])) {
      $name = $valueStep1['name'];
    }
    if ($type == 'user') {
      $name = '';
    }
    if (!in_array($type, ['user', 'field_collection_item'])) {
      $form['collect_field_label']['name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Machine name'),
        '#default_value' => $name,
        '#description' => $this->t('Set your machine name, if new bundle. Set your table name if you want create new table'),
        '#maxlength' => 32,
        '#states' => [
          'visible' => [
            ':input[name="bundle"]' => ['value' => '_none'],
          ],
        ],
      ];
    }
    if (!empty($type) && $type == 'data_custom') {
      $form['collect_field_label']['name']['#title'] = $this->t('Table name');
      $form['collect_field_label']['name']['#description'] = $this->t('Set your table name');
    }

    $form['collect_field_label']['field_exist'] = [
      '#type' => 'button',
      '#value' => $this->t('Fields exist'),
      '#description' => $this->t('If you want load exits field'),
      '#ajax' => [
        'callback' => [$this, 'ajaxQuickDataCollectLabelCallback'],
        'wrapper' => $this->getFormId(),
        'options' => ['query' => ['ajax_form' => 1]],
        'event' => 'click',
      ],
    ];

    $field_label = $form_state->getValue('field_label');
    if (!empty($valueStep1['field_label'])) {
      $field_label = $valueStep1['field_label'];
    }
    $form['collect_field_label']['field_label'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Field label'),
      '#default_value' => $field_label,
      '#description' => $this->t('Enter your list field label'),
    ];
    $userInput = $form_state->getUserInput();
    if (!empty($userInput['type'])) {
      $urlImport = $this->getImportLink($userInput['type'], $userInput['bundle'] ?? NULL);
      if (!empty($urlImport)) {
        $form['collect_field_label']['import'] = [
          '#type' => 'link',
          '#url' => $urlImport,
          '#title' => $this->t('Quick Import data from a CSV file'),
        ];
      }
    }
    if (empty($form['collect_field_label']['field_label']['#default_value'])) {
      if (!empty($form_state->getValue("field_exist"))) {
        $form['collect_field_label']['field_label']['#value'] = $this->loadTypeBunldeFieldsInfo($form_state->getValue("type"), $bundle);
      }
      elseif (isset($form['collect_field_label']['field_label']['#value'])) {
        unset($form['collect_field_label']['field_label']['#value']);
      }
    }
    $generate_module = [
      'search_api' => FALSE,
      'feeds' => FALSE,
      'views' => FALSE,
    ];
    $module_names = [];

    foreach ($generate_module as $module => &$exist) {
      if ($this->moduleHandler->moduleExists($module)) {
        $exist = TRUE;
        $info = $this->moduleExtensionList->getExtensionInfo($module);
        $module_names[$module] = $info['name'];
      }
    }

    if (!empty($module_names)) {
      $form['collect_field_label']['generate'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Extra generate'),
        '#collapsible' => TRUE,
        '#collapsed' => FALSE,
        '#description' => $this->t("Generate with modules %modules", ['%modules' => implode(', ', $module_names)]),
      ];
      foreach ($module_names as $module => $module_name) {
        $form['collect_field_label']['generate'][$module] = [
          '#type' => 'checkbox',
          '#title' => $module_name,
          '#default_value' => $form_state->getValue($module) ?? 0,
        ];
        if ($module == 'search_api') {
          $dataBaseSearch = $this->linkGenerator->generate(
            $this->t('Database Search'),
            Url::fromUri('https://www.drupal.org/project/search_api_db')
          );
          $elasticSearch = $this->linkGenerator->generate(
            $this->t('Elasticsearch'),
            Url::fromUri('https://www.drupal.org/project/search_api_elasticsearch')
          );
          $infoServer = $dataBaseSearch->getGeneratedLink() . ' ' . $this->t('or') . ' ' . $elasticSearch->getGeneratedLink();
          $search_api_server = $this->entityTypeManager->getStorage('search_api_server');
          $servers_search_api = $search_api_server->loadMultiple();
          $optionServerSearch = [];
          if (!empty($servers_search_api)) {
            foreach ($servers_search_api as $search_api) {
              $optionServerSearch[$search_api->id()] = $search_api->label();
            }
          }

          $searchApiServer = $form_state->getValue('SearchApiServer') ?? NULL;
          if (!empty($valueStep1['SearchApiServer'])) {
            $searchApiServer = $valueStep1['SearchApiServer'];
          }
          $form['collect_field_label']['generate']['SearchApiServer'] = [
            '#type' => 'select',
            '#title' => $this->t('Search Server'),
            '#empty_option' => $this->t('- New Search Server -'),
            '#description' => $this->t('Must have server like %info', ['%info' => $infoServer]),
            '#options' => $optionServerSearch,
            '#default_value' => $searchApiServer,
            '#states' => [
              'visible' => [
                "input[name='search_api']" => ["checked" => TRUE],
              ],
            ],
          ];
        }
        // Paragraph don't support feeds.
        if ($module == 'feeds') {
          $form['collect_field_label']['generate'][$module]['#states'] = [
            'invisible' => [
            [':input[name="type"]' => ['value' => 'paragraph']],
              'or',
            [':input[name="type"]' => ['value' => 'data']],
              'or',
            [':input[name="type"]' => ['value' => 'data_custom']],
            ],
          ];
        }
        if ($module == 'search_api') {
          $form['collect_field_label']['generate'][$module]['#states'] = [
            'invisible' => [
            [':input[name="type"]' => ['value' => 'data']],
              'or',
            [':input[name="type"]' => ['value' => 'data_custom']],
            ],
          ];
        }
        if ($module == 'views') {
          $form['collect_field_label']['generate'][$module]['#default_value'] = $form_state->getValue($module) ?? 1;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function step2Form(array &$form, FormStateInterface $form_state) {
    $header = [
      $this->t('Label'),
      $this->t('Machine name'),
      $this->t('Field type'),
      $this->t('Widget'),
    ];
    $field_type_options = [];
    $type = $form_state->getValue('type');
    $type_data = ['data', 'data_custom'];
    if (in_array($type, $type_data)) {
      $header[3] = $this->t('Size');
      if ($type == 'data' && function_exists('data_get_field_types') && function_exists('data_get_field_sizes')) {
        $field_type_options = data_get_field_types();
        $widget_types = data_get_field_sizes();
      }
      else {
        $field_type_options = $this->dataTypeOptions();
        $widget_types = $this->dataSizeOptions();
      }
    }
    else {
      // Gather valid field types.
      $fieldTypePluginManager = $this->fieldTypePluginDefinition;

      foreach ($fieldTypePluginManager->getGroupedDefinitions($fieldTypePluginManager->getUiDefinitions()) as $category => $field_types) {
        foreach ($field_types as $name => $field_type) {
          $extract_field_ui = explode(':', $name);
          if (!empty($extract_field_ui[1])) {
            $fieldUi[$name] = $extract_field_ui[1];
          }
          $field_type_options[$category][$name] = $field_type['label'];
        }
      }
      $widget_types = $this->pluginManager->getOptions();
      if (!empty($fieldUi)) {
        foreach ($fieldUi as $name => $field_type) {
          if (empty($widget_types[$name]) && !empty($widget_types[$field_type])) {
            $widget_types[$name] = $widget_types[$field_type];
          }
        }
      }
    }
    $valueStep1 = $this->store->get('valueStep1');
    $form['table_schema'] = [
      '#type' => 'table',
      '#show_operations' => FALSE,
      '#caption' => $this->t('Create %bundle %type %name', [
        '%bundle' => !empty($valueStep1["bundle"]) && !is_array($valueStep1["bundle"]) && $valueStep1["bundle"] != '_none' ? $valueStep1["bundle"] : '',
        '%type' => $valueStep1["type"],
        '%name' => !empty($valueStep1["name"]) ? $valueStep1["name"] : '',
      ]),
      '#empty' => $this->t('There are no @label yet.', ['@label' => 'fields']),
      '#header' => $header,
      '#attached' => [
        'library' => ['quick_data/drupal.quick_data'],
        'drupalSettings' => [
          'quick_data' => [
            'entity_type' => $form_state->getValue('type'),
          ],
        ],
      ],
    ];

    foreach ($this->load($form_state->getValues(), $form_state->getUserInput()) as $field => $fields) {
      if (empty($field)) {
        continue;
      }
      $table_form = [];
      $table_form['#tree'] = TRUE;

      $table_form['label'] = [
        '#children' => [
          'title' => ['#markup' => $fields['label']],
        ],
        '#type' => 'hidden',
        '#default_value' => $fields['label'],
      ];
      $table_form['name'] = [
        '#children' => [
          'title' => [
            '#markup' => '<small>' . $field . '<span class="admin-link"><a href="#" class="link">Modifier</a></span></small>',
          ],
        ],
        '#type' => 'hidden',
        '#default_value' => $fields['name'],
        '#maxlength' => 26,
      ];
      if (!in_array($fields['name'], [
        'body',
        'comment',
      ]) && !in_array($type, $type_data)) {
        $table_form['name']['#prefix'] = '<span dir="ltr">field_</span>';
      }
      $table_form['type'] = [
        '#type' => 'select',
        '#options' => $field_type_options,
        '#empty_option' => $this->t('- Select a field type -'),
        '#default_value' => $fields['type'],
        '#attributes' => ['class' => ['field-type-select']],
      ];
      $table_form['widget_type'] = [
        '#type' => 'select',
        '#default_value' => $fields['widget_type'],
        '#attributes' => ['class' => ['widget-type-select']],
        '#options' => $widget_types,
        '#empty_option' => $this->t('- Select a field widget -'),
      ];

      $form['table_schema'][$field] = $table_form;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  private function load(array $values, $inputs = []) {
    $form = [];
    $entity_type = $values["type"];
    if (empty($values["field_label"]) && !empty($inputs["field_label"])) {
      $values["field_label"] = $inputs["field_label"];
    }
    if (!empty($values["field_label"])) {
      $machine_name_count =
            $test_exist_machine_name = [];
      $max_length = 26;
      $field_labels = [];
      $test_field_labels = explode(PHP_EOL, $values["field_label"]);
      foreach ($test_field_labels as $test_field_label) {
        $tab_separates = explode("\t", $test_field_label);
        foreach ($tab_separates as $tab_separate) {
          $field_labels[] = $tab_separate;
        }
      }

      foreach ($field_labels as $field_label) :
        $field_type_default = 'string';
        $field_widget_default = 'string_textfield';
        $temp = explode('|', trim($field_label));
        if (!empty($temp[3])) {
          $field_widget_default = trim($temp[3]);
        }
        if (!empty($temp[1])) {
          $field_label = trim($temp[1]);
        }
        if ($entity_type == 'data_custom') {
          $field_type_default = 'varchar';
          $field_widget_default = 'varchar';
        }
        if (!empty($temp[2])) {
          $field_type_default = $type_default = trim($temp[2]);
          if ($entity_type == 'data_custom') {
            $field_type_default = $type_default;
            $field_widget_default = $type_default;
          }
          if ($entity_type == 'data') {
            $field_type_default = $type_default;
            if (!empty($temp[3])) {
              $field_widget_default = trim($temp[3]);
            }
          }
        }
        $machine_name = $this->convertMachineName($temp[0]);
        if (!empty($machine_name)) {
          if (!in_array($machine_name, $test_exist_machine_name)) {
            $test_exist_machine_name[$field_label] = $machine_name;
          }
          else {
            // Add _1 if there is double field name, count 1->9.
            $machine_name = substr($machine_name, 0, $max_length - 2);
            if (empty($machine_name_count[$machine_name])) {
              $machine_name_count[$machine_name] = 1;
            }
            else {
              $machine_name_count[$machine_name] += 1;
            }
            $machine_name = $machine_name . '_' . $machine_name_count[$machine_name];
          }
          if (empty($machine_name)) {
            continue;
          }
          // Check if field exist.
          if (!in_array($entity_type, ['data', 'data_custom'])) {
            $field_storage = $this->entityFieldManager->getFieldStorageDefinitions($entity_type);
            if (empty($field_storage[$machine_name]) && !empty($field_storage['field_' . $machine_name])) {
              $field_name = 'field_' . $machine_name;
              $field_type_default = $field_storage[$field_name]->getType();
              $field_widget_default = $this->getDefaultWidget($field_type_default, $default = "default_widget");
            }
          }
        }
        $form[$machine_name] = [
          'widget_type' => $field_widget_default,
          'type' => $field_type_default,
          'label' => trim($field_label),
          'name' => $machine_name,
        ];
      endforeach;
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxQuickDataCollectFieldCallback(array &$form, FormStateInterface $form_state) {
    if (!empty($form['collect_field_label']['bundle']) && $selectedType = $form_state->getValue('type')) {
      $options = [$form['collect_field_label']['bundle']['#empty_value'] => $form['collect_field_label']['bundle']['#empty_option']] + $this->collectFieldLabelType($selectedType);
      if (!empty($options)) {
        $form['collect_field_label']['bundle']['#options'] = $options;
      }
    }
    return $form['collect_field_label'];
  }

  /**
   * {@inheritdoc}
   */
  public function ajaxQuickDataCollectLabelCallback(&$form, &$form_state) {
    $form['collect_field_label']['field_label']['#value'] = $this->loadTypeBunldeFieldsInfo($form_state->getValue("type"), $form_state->getValue("bundle"));
    return $form['collect_field_label'];
  }

  /**
   * {@inheritdoc}
   */
  private function quickDataCreateFieldDataSubmit(array &$form, FormStateInterface $form_state, $valueStep1) {
    $values = $form_state->getValue('table_schema') ?? FALSE;
    $bundle = !empty($valueStep1['bundle']) && !is_array($valueStep1['bundle']) && $valueStep1['bundle'] != '_none' ? $valueStep1['bundle'] : NULL;
    $new_name = !empty($valueStep1['name']) ? $valueStep1['name'] : 'user';
    $description = $this->t("Generate @name by quick data", ["@name" => $new_name]);
    if (empty($bundle)) {
      $transliteration = new PhpTransliteration();
      $name = $transliteration->transliterate($new_name);
      $bundle = str_replace(' ', '_', strtolower($name));
    }
    else {
      $name = ucfirst(str_replace('_', ' ', $bundle));
    }
    $entity_type = $valueStep1['type'];
    if ($entity_type == 'user') {
      $bundle = 'user';
    }
    $langcode = $this->languageManager->getDefaultLanguage()->getId();

    switch ($entity_type) {
      case 'user':
      case 'field_collection_item':
        $this->addFields($bundle, $entity_type, $values, $langcode);
        break;

      case 'node':
        $this->setupNodeType($bundle, $new_name, $description, $langcode);
        $this->addFields($bundle, $entity_type, $values, $langcode);
        break;

      case 'taxonomy_term':
        $this->setupTaxonomyVocabulary($bundle, $new_name, $description, $langcode);
        $this->addFields($bundle, $entity_type, $values, $langcode);
        break;

      case 'data':
        $schema = $this->schemaData($values);
        $storage = $this->entityTypeManager->getStorage('data_table_config');
        $table = $storage->load($bundle);
        if (!$table) {
          $table = $storage->create([
            'id' => $bundle,
            'description' => 'Short table description',
            'table_schema' => $schema,
          ]);
          $table->save();
        }
        else {
          $this->dataAddField($bundle, $schema, $new_name);
        }
        break;

      case 'data_custom':
        $this->createTableDataCustom($bundle, $values, $new_name, $description);
        break;

      default:
        $this->setupEntityType($entity_type, $bundle, $new_name, $description, $langcode);
        $this->addFields($bundle, $entity_type, $values, $langcode);
        break;
    }
    $generate_views = TRUE;
    if (!empty($valueStep1["search_api"])) {
      if (empty($valueStep1['SearchApiServer'])) {
        $server = $this->createSearchApiServer($bundle, $name, $description, $langcode);
      }
      else {
        $server = $this->entityTypeManager->getStorage('search_api_server')->load($valueStep1['SearchApiServer']);
      }
      if (!empty($server)) {
        $index = $this->entityTypeManager->getStorage('search_api_index')->load($valueStep1['SearchApiServer']);
        if (empty($index)) {
          $index = $this->createSearchApiIndex($entity_type, $bundle, $name, $description, $values, $langcode);
        }
      }
      if (!empty($valueStep1["views"])) {
        // @todo create search api views
        // $generate_views = FALSE;
      }
    }
    $functionName = str_replace('_', '', ucfirst($entity_type));
    if ($generate_views && !empty($valueStep1["views"])) {
      $function = 'set' . $functionName . 'View';
      if (method_exists($this, $function)) {
        $this->$function($bundle, $name, $values, $langcode);
      }
    }
    if (!empty($valueStep1["feeds"])) {
      $function = 'set' . $functionName . 'Feeds';
      if (method_exists($this, $function)) {
        $this->$function($bundle, $name, $values, $langcode);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  private function addFields($bundle, $entity_type, $fields, $langcode = 'en') {

    $field_storages = $this->entityFieldManager->getFieldDefinitions($entity_type, $bundle);
    $new_fields = [];
    foreach ($fields as $field_name => $field_setting) {
      if (!empty($field_setting["name"])) {
        $field_name = str_replace(' ', '_', trim(strtolower($field_setting["name"])));
      }
      if (empty($field_name) || !empty($field_storages[$field_name]) || !empty($field_storages['field_' . $field_name])) {
        continue;
      }
      $field_setting['name'] = 'field_' . $field_name;
      $cardinality = 1;
      $settings = [];
      $type = $field_setting['type'];
      $extract_field_ui = explode(':', $type);
      if (!empty($extract_field_ui[1])) {
        $type = $extract_field_ui[1];
        if (!empty($extract_field_ui[2])) {
          $settings['target_type'] = $extract_field_ui[2];
          $field_setting["settings"] = ['handler' => 'default:' . $extract_field_ui[2]];
          $description = implode(' ', [
            $entity_type,
            $bundle,
            'Field:',
            $field_setting["label"],
          ]);
          $this->setupEntityType($extract_field_ui[2], $field_name, $field_setting["label"], $description, $langcode);
          $field_setting["settings"]['handler_settings'] = [
            'target_bundles' => [$field_name => $field_name],
            'target_bundles_drag_drop' => [
              $field_name => ['enabled' => TRUE],
            ],
          ];
        }
        $cardinality = -1;
      }
      $this->setupFieldStorage($field_setting['name'], $entity_type, $langcode, $type, $cardinality, $settings);
      $field = $this->createEntityField($entity_type, $bundle, $field_setting);
      if ($field) {
        $new_fields[] = $field_name;
      }
    }
    if (!empty($new_fields)) {
      $this->messenger()->addMessage(
        $this->t("Add new fields %fields to %entity_type %bundle", [
          '%fields' => implode(', ', $new_fields),
          '%entity_type' => $entity_type,
          '%bundle' => $bundle,
        ]),
        'status',
        TRUE
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  private function collectFieldLabelType($type = FALSE) {
    $bundles = [];
    if (!empty($type)) {
      switch ($type) {
        case 'user':
          break;

        case 'data':
          $list = $this->entityTypeManager->getListBuilder('data_table_config');
          foreach ($list->load() as $entity) {
            $bundles[$entity->id()] = $entity->label();
          }
          break;

        case 'data_custom':
          $list_tables = $this->config('quick_data.settings')->get('tables');
          if (!empty($list_tables)) {
            $bundles = array_combine($list_tables, $list_tables);
          }
          break;

        default:
          $entity_types = $this->entityTypeBundleInfo->getBundleInfo($type);
          if (!empty($entity_types)) {
            foreach ($entity_types as $key => $name) {
              $bundles[$key] = !empty($name['label']) ? (string) $name['label'] : NULL;
            }
          }
      }
    }
    return $bundles;
  }

  /**
   * {@inheritdoc}
   */
  private function loadTypeBunldeFieldsInfo($type, $bundle) {
    if ($type == "user") {
      $bundle = 'user';
    }
    if ($bundle == '_none') {
      return NULL;
    }
    if (empty($type) || empty($bundle)) {
      return NULL;
    }
    $infos = [];
    if ($type == 'data_custom' && !empty($bundle)) {
      $fields = $this->getTableSchema($bundle);
      foreach ($fields as $field_name => $field_config) {
        $config = explode('|', $field_config);
        $infos[] = implode('|', [$field_name, $config[1], $config[0]]);
      }
      return implode(PHP_EOL, $infos);
    }

    if ($type == 'data' && !empty($bundle)) {
      // Get fields from entity data.
      $schema = $this->tableSchema($bundle);
      $fields = $schema['fields'];
      foreach ($fields as $field_name => $field_config) {
        $infos[] = implode('|', [
          $field_name,
          $field_config['label'],
          $field_config['type'],
          $field_config['size'],
        ]);
      }
      return implode(PHP_EOL, $infos);
    }

    $form_mode = 'default';
    $form_display = $this->entityTypeManager->getStorage('entity_form_display')->load($type . '.' . $bundle . '.' . $form_mode);
    if (empty($form_display)) {
      return NULL;
    }
    $fields = $this->entityFieldManager->getFieldDefinitions($type, $bundle);
    $typeExcept = ['feeds_item', 'entity_reference_revisions', 'comment'];
    foreach ($fields as $field_config) {
      if ($field_config instanceof FieldConfig) {
        $specific_widget_type = $form_display->getComponent($field_config->getName());
        $field_type = $field_config->getSettings('type');
        $field_storage = $field_config->getFieldStorageDefinition();
        $type = $field_config->getType();
        if (!empty($field_type) && !in_array($type, $typeExcept)) {
          $infos[] = [
            'title' => implode('|', [
              $field_config->getName(),
              $field_config->getLabel(),
              $field_storage->getType(),
              $specific_widget_type["type"],
            ]),
            'weight' => $specific_widget_type["weight"],
          ];
        }
      }
    }
    $out = [];
    if (!empty($infos)) {
      uasort($infos, [
        'Drupal\Component\Utility\SortArray',
        'sortByWeightElement',
      ]);
      foreach ($infos as $info) {
        $out[] = $info['title'];
      }
    }
    return implode(PHP_EOL, $out);
  }

  /**
   * {@inheritdoc}
   */
  protected function setupNodeType($bundle, $new_name, $description = '', $langcode = 'en') {

    $content_type = $this->entityTypeManager->getStorage('node_type')->load($bundle);
    if (!empty($content_type)) {
      return $content_type;
    }
    $config = $this->config('quick_data.nodeType.default');
    $default = [
      'type' => $bundle,
      'label' => $new_name,
      'name' => $new_name,
      'description' => $description,
      'langcode' => $langcode,
    ] + $config->getRawData();
    unset($default["dependencies"]);

    $content_type = NodeType::create($default);

    $content_type->save();
    $this->messenger()->addStatus($this->t('The content type %name has been added.', ['%name' => $bundle]));
    $fields = $this->entityFieldManager->getFieldDefinitions('node', $content_type->id());
    if (!empty($fields)) {
      $options = $default['options'];
      foreach ($options as $field_name => $value) {
        if (!empty($fields[$field_name])) {
          $fields[$field_name]->getConfig($content_type->id())->setDefaultValue($value)->save();
        }
      }
    }
    return $content_type;
  }

  /**
   * {@inheritdoc}
   */
  private function setupFieldStorage(string $fieldName, string $entity_type = 'node', string $langcode = 'en', string $field_type = '', int $cardinality = 1, $settings = []) {
    $field_storage = FieldStorageConfig::loadByName($entity_type, $fieldName);
    if (!empty($field_storage)) {
      return $field_storage;
    }
    $field_config = [
      'field_name' => $fieldName,
      'langcode' => $langcode,
      'entity_type' => $entity_type,
      'cardinality' => $cardinality,
      'type' => $field_type,
      'locked' => FALSE,
      'translatable' => TRUE,
      'persist_with_no_fields' => FALSE,
      'custom_storage' => FALSE,
    ];
    if (!empty($settings)) {
      $field_config['settings'] = $settings;
    }
    $field_storage = FieldStorageConfig::create($field_config)->save();
    return $field_storage;
  }

  /**
   * {@inheritdoc}
   */
  private function createEntityField($entity_type, $bundle, $field_setting) {
    $field_name = $field_setting['name'];
    $field_label = trim($field_setting['label']);
    $field_type = $type = $field_setting['type'];
    $extract_field_ui = explode(':', $type);
    if (!empty($extract_field_ui[1])) {
      $type = $extract_field_ui[1];
    }
    $field_config = [
      'field_name' => $field_name,
      'entity_type' => $entity_type,
      'bundle' => $bundle,
      'label' => $field_label,
      'required' => FALSE,
      'translatable' => TRUE,
      'field_storage' => FieldStorageConfig::loadByName($entity_type, $field_name),
      'field_type' => $type,
    ];
    if (!empty($field_setting['settings'])) {
      $field_config['settings'] = $field_setting['settings'];
    }
    $field = FieldConfig::loadByName($entity_type, $bundle, $field_name);

    if (empty($field)) {
      $field = FieldConfig::create($field_config);
      if (!empty($field)) {
        $field->save();
      }
      $form_mode = $view_mode = 'default';
      $entity_display = [
        'targetEntityType' => $entity_type,
        'bundle' => $bundle,
        'mode' => $form_mode,
        'status' => TRUE,
      ];

      $entity_form_display = $this->entityTypeManager->getStorage('entity_form_display')->load($entity_type . '.' . $bundle . '.' . $form_mode);
      if (!$entity_form_display) {
        $entity_form_display = $this->entityTypeManager->getStorage('entity_form_display')->create($entity_display);
      }
      $entity_form_display->setComponent($field_name, ['type' => $this->getDefaultWidget($field_type)])->save();

      $entity_view_display = $this->entityTypeManager->getStorage('entity_view_display')->load($entity_type . '.' . $bundle . '.' . $view_mode);
      if (!$entity_view_display) {
        $entity_view_display = $this->entityTypeManager->getStorage('entity_view_display')->create($entity_display);
      }
      $entity_view_display->setComponent($field_name, ['type' => $this->getDefaultWidget($field_type, "default_formatter")])
        ->save();
      return $field;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultWidget($field_type, $default = "default_widget") {
    // $this->fieldTypePluginDefinition = $this->fieldTypePluginDefinition ??
    // \Drupal::service('plugin.manager.field.field_type');
    $definition = $this->fieldTypePluginDefinition->getDefinitions();
    return !empty($definition[$field_type]) ? $definition[$field_type][$default] : FALSE;
  }

  /**
   * {@inheritdoc}
   */
  private function setupTaxonomyVocabulary($bundle, $new_name, $description = '', $langcode = 'en') {
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->load($bundle);
    if (empty($vocabulary)) {
      $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->create([
        'vid' => $bundle,
        'description' => $description,
        'name' => $new_name,
      ]);
      $vocabulary->save();
      $this->messenger()->addStatus($this->t('Created new vocabulary %name.', [
        '%name' => $new_name,
      ]));
    }
    return $vocabulary;
  }

  /**
   * {@inheritdoc}
   */
  private function setupEntityType($entity_type, $bundle, $new_name, $description = '', $langcode = 'en') {
    $entity_storage = $this->entityTypeManager->getStorage($entity_type);
    $entityType = $entity_storage->load($bundle);
    if (empty($entityType)) {
      $entity = [
        'type' => $bundle,
        'id' => $bundle,
        'description' => $description,
        'label' => $new_name,
        'multiple' => TRUE,
      ];
      if ($entity_type == 'profile') {
        $entity['registration'] = TRUE;
        $entity['roles'] = ['authenticated' => 'authenticated'];
      }
      if ($entity_type == 'taxonomy_term') {
        $entity['name'] = $bundle;
        $entity['vid'] = !empty($bundle) ? $bundle : 'tags';
      }
      $entityType = $entity_storage->create($entity);
      $entityType->save();
      $this->messenger()->addStatus($this->t('Saved the %label type.', [
        '%label' => $new_name,
      ]));
    }
    return $entityType;
  }

  /**
   * Helper convert name to machine_name must be less than 26 characters.
   */
  public function convertMachineName($label, $maxlen = 26) {
    $field_label = str_replace(["field_", "'", '"'], " ", trim($label));
    $transliteration = new PhpTransliteration();
    $machine_name = $transliteration->transliterate($field_label);
    $stop_words = $this->config('quick_data.settings')->get('stopwords');
    $lang_code = $this->languageManager->getCurrentLanguage()->getId();
    $remove_words = $stop_words[$lang_code] ?? NULL;

    if (!empty($remove_words)) {
      $words = explode(' ', strtolower($machine_name));
      if ($words[0] == 'field') {
        unset($words[0]);
      }
      $field_label = array_diff($words, $remove_words);
      $machine_name = implode(' ', $field_label);
      if (empty($machine_name)) {
        $machine_name = $label;
      }
    }
    $machine_name = preg_replace('/[^a-z0-9_ ]/i', '', trim($machine_name));

    $machine_name = substr($machine_name, 0, $maxlen);
    return $machine_name = str_replace(['-', ' '], '_', $machine_name);
  }

  /**
   * {@inheritdoc}
   */
  private function dataTypeOptions() {
    $types = array_keys($this->dataSizeOptions());
    return array_combine($types, $types);
  }

  /**
   * {@inheritdoc}
   */
  private function dataSizeOptions() {
    $sizes = $this->config('quick_data.settings')->get('data_custom_type');
    foreach ($sizes as $key => $val) {
      $sizes[$key] = array_combine($val, $val);
    }
    return $sizes;
  }

  /**
   * {@inheritdoc}
   */
  public function dataTypeConvertSchema() {
    $sizes = $this->config('quick_data.settings')->get('schema_size');
    $type_connection = $this->database->databaseType();
    if ($type_connection !== 'mysql') {
      $sizes['json'] = $sizes['longtext'];
      $sizes['real'] = $sizes['float'];
      if ($type_connection == 'pgsql') {
        $sizes['datetime'] = $sizes['timestamp'];
        $sizes['geometrycollection'] = $sizes['point'];
        $sizes['linestring']['type'] = 'line';
        $sizes['multilinestring']['type'] = 'path';
        $sizes['multipolygon']['type'] = 'box';
      }
    }
    return $sizes;
  }

  /**
   * {@inheritdoc}
   */
  private function tableSchema($table_schema, $description = '') {
    $schema = [
      'description' => $description,
      'fields' => [],
      'indexes' => ['id' => ['id']],
      'primary key' => ['id'],
      'auto increment' => 1,
    ];
    $have_primary = FALSE;
    $type_connection = $this->database->databaseType();
    foreach ($table_schema as $field_name => $field_setting) {
      $field_name = str_replace(' ', '_', trim(strtolower($field_name)));
      $schema['fields'][$field_name] = [
        'name' => $field_name,
        'label' => trim($field_setting['label']),
        'type' => $field_setting['type'],
        'size' => $field_setting['widget_type'] ?? NULL,
        'length' => NULL,
        'description' => trim($field_setting['label']),
        'primary' => FALSE,
        'index' => FALSE,
        'unsigned' => FALSE,
      ];
      if (!empty($field_setting[$type_connection . '_type'])) {
        $schema['fields'][$field_name][$type_connection . '_type'] = $field_setting[$type_connection . '_type'];
      }
      // Get DatabaseSchema::getFieldTypeMap() for maping.
      if (in_array($field_setting['type'], ['varchar'])) {
        $schema['fields'][$field_name]['length'] = 255;
        $size = ['tiny' => 32, 'small' => 255, 'medium' => 1024];
        if (!empty($field_setting['widget_type'])) {
          if (!empty($size[$field_setting['widget_type']])) {
            $schema['fields'][$field_name]['length'] = $size[$field_setting['widget_type']];
          }
          if ($field_setting['widget_type'] == 'tiny') {
            $schema['fields'][$field_name]['type'] = 'char';
          }
          if ($field_setting['widget_type'] == 'small') {
            $schema['fields'][$field_name]['type'] = 'varchar_ascii';
          }
          if ($field_setting['widget_type'] == 'big') {
            $schema['fields'][$field_name]['type'] = 'text';
          }
        }

        $schema['fields'][$field_name]['size'] = 'normal';
      }
      if ($field_setting['type'] == 'text') {
        $schema['indexes']['id'][] = $field_name;
      }
      if ($field_setting['type'] == 'serial') {
        $have_primary = TRUE;
        $schema['fields'][$field_name]['primary'] = TRUE;
        $schema['fields'][$field_name]['serialize'] = TRUE;
        $schema['fields'][$field_name]['not null'] = TRUE;
        $schema['fields'][$field_name]['unsigned'] = TRUE;
        if (empty($schema['fields'][$field_name]['size'])) {
          $schema['fields'][$field_name]['size'] = 'normal';
        }
      }
      if (in_array($field_setting['type'], ['int', 'float'])) {
        if (in_array($field_setting['widget_type'], ['big'])) {
          $schema['fields'][$field_name]['unsigned'] = TRUE;
        }
      }

      if ($field_setting['type'] == 'date') {
        $schema['fields'][$field_name]['type'] = 'varchar';
        $size = [
          'small' => 'DATE',
          'tiny' => 'TIME',
          'normal' => 'DATETIME',
          'medium' => 'TIMESTAMP',
          'big' => 'YEAR',
        ];
        if (!empty($field_setting['widget_type']) && !empty($size[$field_setting['widget_type']])) {
          $schema['fields'][$field_name]['mysql_type'] = $size[$field_setting['widget_type']];
        }
      }
    }
    // Auto add id if without primary.
    if (!$have_primary && empty($table_schema['id'])) {
      $schema['fields']['id'] = [
        'name' => 'id',
        'label' => 'id',
        'type' => 'serial',
        'size' => NULL,
        'length' => NULL,
        'unsigned' => TRUE,
        'not null' => TRUE,
        'primary' => TRUE,
      ];
    }
    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  private function schemaData($table_name) {
    $schema = $this->tableSchema($table_name);
    $fields_config = FALSE;
    if (!empty($schema['fields'])) {
      $fields_config = array_values($schema['fields']);
    }
    return $fields_config;
  }

  /**
   * {@inheritdoc}
   */
  private function createTableDataCustom(string $table, array $table_schema, $new_name = '', $description = '') {
    $schemaTypeConvert = $this->dataTypeConvertSchema();
    $type_connection = $this->database->databaseType();
    $schema = $this->database->schema();
    $table_exits = FALSE;
    if (!empty($schema->tableExists($table))) {
      $table_exits = TRUE;
      $fields = $this->getTableSchema($table);
    }
    foreach ($table_schema as $field_name => &$field_setting) {
      $key = $field_setting['widget_type'];
      unset($field_setting['widget_type']);
      $field_setting['type'] = $schemaTypeConvert[$key]['type'];
      if (!empty($schemaTypeConvert[$key]['size'])) {
        $field_setting['size'] = $schemaTypeConvert[$key]['size'];
      }
      else {
        $field_setting[$type_connection . '_type'] = $schemaTypeConvert[$key]['type'];
        $field_setting['size'] = NULL;
      }
      if (!empty($fields[$field_name])) {
        unset($table_schema[$field_name]);
        continue;
      }
    }
    $table_definition = $this->tableSchema($table_schema, $description);
    if (!$table_exits) {
      $schema->createTable($table, $table_definition);
      $list_tables = $this->config('quick_data.settings')->get('tables');
      $list_tables[] = $table;
      $config = $this->configFactory->getEditable('quick_data.settings');
      $config->set('tables', array_unique($list_tables))->save();
    }
    else {
      foreach ($table_schema as $field_name => $field_setting) {
        $schema->addField($table, $field_name, $field_setting);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getTableSchema($table) {
    // Verify the field type on the data column in the cache table.
    // @todo this is MySQL specific.
    $query = $this->database->query("SHOW FULL COLUMNS FROM {" . $table . "}");
    $definition = [];
    while ($row = $query->fetchAssoc()) {
      $extract_type = explode('(', $row['Type']);
      $definition[$row['Field']] = $extract_type[0] . '|' . $row['Comment'];
    }
    return $definition;
  }

  /**
   * {@inheritdoc}
   */
  private function dataAddField(string $table_name, array $schema) {
    // @phpstan-ignore-next-line
    $table = new Table($table_name);
    $storage = $this->entityTypeManager->getStorage('data_table_config')->load($table_name);
    $list_fields = $this->getTableSchema($table_name);
    foreach ($schema as $spec) {
      if (empty($list_fields[$spec['name']])) {
        Database::getConnection()->schema()->addField($table_name, $spec['name'], $spec);
        $storage->table_schema[] = $spec;
      }
    }
    $storage->save();
    return $table;
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityView($bundle, $name, array $fields, $langcode = 'en', $type = 'node') {
    $storage_view = $this->entityTypeManager->getStorage('view');
    if (!empty($entityView = $storage_view->load($bundle))) {
      $url = $entityView->toUrl('edit-form');
      $view_table = Link::fromTextAndUrl($bundle, $url)->toRenderable();
      $this->messenger()->addMessage(
        $this->t("Views %viewname already exists", ['%viewname' => $this->renderer->render($view_table)]),
        'error'
      );
      return;
    }

    $view = $this->config('quick_data.views_' . $type . '.default')
      ->getRawData();
    unset($view["dependencies"]);
    $view_page = $view["display"]["page"];
    unset($view["display"]["page"]);
    $view['langcode'] = $langcode;
    $view_page['id'] = $view['id'] = $bundle;
    $view_page['display_title'] = $view['label'] = $name;
    $view_page['display_options']['path'] = $bundle;

    $filter = 'type';
    $field_name_default = 'title';
    switch ($type) {
      case 'taxonomy_term':
        $filter = 'vid';
        $field_name_default = 'name';
        break;

      case 'field_collection_item':
        $filter = 'field_name';
        $field_name_default = 'item_id';
        break;

      case 'paragraph':
        $field_name_default = 'id';
        $view['config'] = ['paragraphs.paragraphs_type.' . $bundle];
        break;
    }

    $default_field_config = $view["display"]["default"]["display_options"]["fields"][$field_name_default];
    unset($default_field_config['settings']);
    if (!empty($filter)) {
      $view["display"]["default"]["display_options"]["filters"][$filter]["value"] = [$bundle => $bundle];
    }
    $view["display"]["default"]["display_options"]["title"] = $name;
    if ($type == 'node') {
      $field_storages = $this->entityFieldManager->getFieldStorageDefinitions($type, $bundle);
    }
    else {
      $field_storages = $this->entityFieldManager->getFieldStorageDefinitions($type);
    }

    foreach ($fields as $field_name => $field_setting) {
      if (empty($field_storages[$field_name]) || !empty($field_storages['field_' . $field_name])) {
        $field_name = 'field_' . $field_name;
      }
      $default_field_config['id'] = $field_name;
      $default_field_config['label'] = $field_setting['label'];
      $default_field_config['table'] = $type . '__' . $field_name;
      $default_field_config['field'] = $field_name;
      $default_field_config['exclude'] = FALSE;
      $default_field_config['type'] = $this->getDefaultWidget($field_setting['type'], 'default_formatter');
      $view["display"]["default"]["display_options"]["fields"][$field_name] = $default_field_config;
    }
    $view["display"][$bundle] = $view_page;
    $storage_view->create($view)->save();
    if (!empty($entityView = $storage_view->load($bundle))) {
      $url = $entityView->toUrl('edit-form');
      $view_table = Link::fromTextAndUrl($bundle, $url)->toRenderable();
      $this->messenger()->addMessage(
        $this->t("Create new Views %viewname", ['%viewname' => $this->renderer->render($view_table)]),
        'status',
        TRUE
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setUserView($bundle, $name, array $fields, $langcode = 'en') {
    $view = $this->config('quick_data.views_user.default')->getRawData();
    unset($view["dependencies"]["enforced"]);
    $view['langcode'] = $langcode;
    $default_field_config = $view["display"]["default"]["display_options"]["fields"]["name"];
    unset($default_field_config['settings']);
    $field_storages = $this->entityFieldManager->getFieldStorageDefinitions($bundle);
    $storage_view = $this->entityTypeManager->getStorage('view');
    foreach ($fields as $field_name => $field_setting) {
      if (empty($field_storages[$field_name]) || !empty($field_storages['field_' . $field_name])) {
        $field_name = 'field_' . $field_name;
      }
      if (!empty($view["display"]["default"]["display_options"]["fields"][$field_name])) {
        continue;
      }
      $default_field_config['id'] = $field_name;
      $default_field_config['label'] = $field_setting['label'];
      $default_field_config['table'] = 'user__' . $field_name;
      $default_field_config['field'] = $field_name;
      $default_field_config['type'] = $this->getDefaultWidget($field_setting['widget_type']);
      $view["display"]["default"]["display_options"]["fields"][$field_name] = $default_field_config;
    }
    if (empty($storage_view->load($view['id']))) {
      $storage_view->create($view)->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setNodeView($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityView($bundle, $name, $fields, $langcode, 'node');
  }

  /**
   * {@inheritdoc}
   */
  protected function setTaxonomytermView($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityView($bundle, $name, $fields, $langcode, 'taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  protected function setFieldcollectionitemView($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityView($bundle, $name, $fields, $langcode, 'field_collection_item');
  }

  /**
   * {@inheritdoc}
   */
  protected function setParagraphView($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityView($bundle, $name, $fields, $langcode, 'paragraph');
  }

  /**
   * {@inheritdoc}
   */
  protected function setDatacustomView($table_name, $name, array $fields, $langcode = 'en') {
    $storage_view = $this->entityTypeManager->getStorage('view');
    if (!empty($entityView = $storage_view->load($table_name))) {
      $url = $entityView->toUrl('edit-form');
      $view_table = Link::fromTextAndUrl($table_name, $url)->toRenderable();
      $this->messenger()->addMessage(
        $this->t("Views %viewname already exists", ['%viewname' => $this->renderer->render($view_table)]),
        'error'
        );
      return;
    }
    if (!$this->moduleHandler->moduleExists('view_custom_table')) {
      $this->messenger()
        ->addMessage($this->t('Please install module <a href="https://www.drupal.org/project/view_custom_table">view_custom_table</a> to generate'), 'error');
      return;
    }
    $table_database = 'default';
    $description = $name . ' is generated by quick data';
    $serialize_relations = serialize([]);

    $config_view_custom_table = $this->configFactory->getEditable('view_custom_table.tables');

    $config_view_custom_table->set($table_name . '.table_name', $table_name)
      ->set($table_name . '.table_database', $table_database)
      ->set($table_name . '.description', $description)
      ->set($table_name . '.column_relations', $serialize_relations)
      ->set($table_name . '.created_by', $this->currentUser()->id());
    $config_view_custom_table->save();

    $url = Url::fromRoute('view_custom_table.edittablerelations', ['table_name' => $table_name]);
    $edit_relations = Link::fromTextAndUrl($table_name, $url)->toRenderable();
    $this->messenger()->addMessage(
      $this->t('Please add relations %relation Edit Custom Table Relations', [
        '%relation' => $this->renderer->render($edit_relations),
      ]), 'warning');

    $view = $this->config('quick_data.views_custom_data.default')->getRawData();
    unset($view["dependencies"]);
    $view_page = $view["display"]["page"];
    unset($view["display"]["page"]);
    $view['langcode'] = $langcode;
    $view_page['id'] = $view['id'] = $view['base_table'] = $table_name;
    $view_page['display_title'] = $view['label'] = $name;
    $view_page['display_options']['path'] = $table_name;
    $view["display"]["default"]["display_options"]["fields"]['id']['table'] = $table_name;
    $default_field_config = $view["display"]["default"]["display_options"]["fields"]['id'];

    $view["display"]["default"]["display_options"]["title"] = $name;
    foreach ($fields as $field_name => $field_setting) {
      $default_field_config['id'] = $field_name;
      $default_field_config['label'] = $field_setting['label'];
      $default_field_config['field'] = $field_name;
      $view["display"]["default"]["display_options"]["fields"][$field_name] = $default_field_config;
    }
    $view["display"][$table_name] = $view_page;

    $storage_view->create($view)->save();
    if (!empty($entityView = $storage_view->load($table_name))) {
      $url = $entityView->toUrl('edit-form');
      $view_table = Link::fromTextAndUrl($table_name, $url)->toRenderable();
      $this->messenger()->addMessage(
        $this->t("Create new Views %viewname", ['%viewname' => $this->renderer->render($view_table)]),
        'status',
        TRUE
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setEntityFeeds($bundle, $name, array $fields, $langcode = 'en', $type = 'node') {
    $storage_feed = $this->entityTypeManager->getStorage('feeds_feed_type');
    if (!empty($entityFeed = $storage_feed->load($bundle))) {
      $url = $entityFeed->toUrl('edit-form');
      $feeds_table = Link::fromTextAndUrl($bundle, $url)->toRenderable();
      $this->messenger()->addMessage(
        $this->t("Feeds %feedsname already exists", ['%feedsname' => $this->renderer->render($feeds_table)]),
        'error'
      );
      return;
    }

    $feeds = $this->config('quick_data.feed_' . $type . '.default')->getRawData();
    unset($feeds["dependencies"]);

    $feeds["label"] = 'Create ' . $bundle . ' feed';
    $feeds["id"] = $bundle;
    $processor_type = "type";
    switch ($bundle) {
      case 'taxonomy_term':
        $processor_type = 'vid';
        break;

      case 'field_collection_item':
        $processor_type = 'field_name';
        break;
    }
    if ($bundle != 'user') {
      $feeds["processor_configuration"]["values"][$processor_type] = $bundle;
    }
    if ($type == 'node') {
      $field_storages = $this->entityFieldManager->getFieldStorageDefinitions($bundle);
    }
    else {
      $field_storages = $this->entityFieldManager->getFieldStorageDefinitions($type);
    }

    $settings_text = ['format' => 'plain_text'];
    $settings_date = ['timezone' => 'UTC'];
    $dateType = ['datetime', 'timestamp', 'daterange'];
    $settings_file = [
      'reference_by' => 'filename',
      'existing' => 2,
      'autocreate' => FALSE,
    ];
    foreach ($fields as $field_name => $field_setting) {
      if (empty($field_storages[$field_name]) || !empty($field_storages['field_' . $field_name])) {
        $field_name = 'field_' . $field_name;
      }
      $mappings_val = 'value';
      $settings = $settings_text;
      if (in_array($field_setting['type'], ['image', 'file'])) {
        $settings = $settings_file;
        $mappings_val = 'target_id';
      }
      if (in_array($field_setting['type'], $dateType)) {
        $settings = $settings_date;
      }
      $mappings = [
        'target' => $field_name,
        'map' => [
          $mappings_val => $field_name,
          'settings' => $settings,
        ],
      ];
      $feeds['custom_sources'][$field_name] = [
        'label' => $field_setting['label'],
        'value' => $field_setting['label'],
        'machine_name' => $field_name,
      ];
      $feeds["mappings"][] = $mappings;
    }

    $storage_feed->create($feeds)->save();
    if (!empty($entityFeed = $storage_feed->load($bundle))) {
      $url = $entityFeed->toUrl('edit-form');
      $feeds_table = Link::fromTextAndUrl($bundle, $url)->toRenderable();
      $this->messenger()->addMessage(
        $this->t("Create new Feed %feedsname", [
          '%feedsname' => $this->renderer->render($feeds_table),
        ]),
        'status',
        TRUE
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function setNodeFeeds($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityFeeds($bundle, $name, $fields, $langcode, 'node');
  }

  /**
   * {@inheritdoc}
   */
  protected function setUserFeeds($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityFeeds($bundle, $name, $fields, $langcode, 'user');
  }

  /**
   * {@inheritdoc}
   */
  protected function setFieldcollectionitemFeeds($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityFeeds($bundle, $name, $fields, $langcode, 'field_collection_item');
  }

  /**
   * {@inheritdoc}
   */
  protected function setParagraphFeeds($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityFeeds($bundle, $name, $fields, $langcode, 'paragraph');
  }

  /**
   * {@inheritdoc}
   */
  protected function setTaxonomytermFeeds($bundle, $name, array $fields, $langcode = 'en') {
    $this->setEntityFeeds($bundle, $name, $fields, $langcode, 'taxonomy_term');
  }

  /**
   * {@inheritDoc}
   */
  private function createSearchApiServer(string $bundle, string $name, $description = '', string $langcode = 'en') {
    $search_api_class = FALSE;
    if ($this->moduleHandler->moduleExists('search_api_db')) {
      $search_api_class = 'search_api_db';
    }
    /*
     * todo add service for elasticsearch_connector
    if (\Drupal::moduleHandler()->moduleExists('elasticsearch_connector')) {
    $search_api_class = 'search_api_elasticsearch_service';
    }*/
    if ($search_api_class) {
      $config = $this->config('quick_data.search_api.default');
      $default = [
        'id' => $bundle,
        'name' => ucfirst($name),
        'description' => $description,
        'backend' => $search_api_class,
        'langcode' => $langcode,
      ] + $config->getRawData();
      $url = Url::fromUri('internal:/admin/config/search/search-api/server/' . $bundle . '/edit');
      $serverapi = Link::fromTextAndUrl($name, $url)->toRenderable();
      $this->messenger()->addMessage(
        $this->t("Create new search server %serverapi", ['%serverapi' => $this->renderer->render($serverapi)]),
        'status',
        TRUE
      );
      return $this->entityTypeManager->getStorage('search_api_server')->create($default)->save();
    }
  }

  /**
   * {@inheritDoc}
   */
  private function createSearchApiIndex($entity_type, $bundle, string $name, $description = '', $fields = [], string $langcode = 'en') {
    $config = $this->config('quick_data.search_api_index.default');

    $default = [
      'id' => $bundle,
      'name' => ucfirst($name),
      'description' => $description,
      'langcode' => $langcode,
      'server' => $bundle,
    ] + $config->getRawData();
    $data_setting = $default['datasource_settings']['entity:node'];
    unset($default['datasource_settings']['entity:node']);
    $data_setting['bundles']['selected'] = [$bundle];
    $datasource_id = 'entity:' . $entity_type;
    $default['datasource_settings'][$datasource_id] = $data_setting;
    if (!empty($fields)) {
      $default['field_settings'] = $this->addFieldsIndex($fields, $datasource_id);
    }
    $url = Url::fromUri('internal:/admin/config/search/search-api/index/' . $bundle . '/edit');
    $serverapiIndex = Link::fromTextAndUrl($name, $url)->toRenderable();
    $this->messenger()->addMessage(
      $this->t("Create new search index%serverapi", ['%serverapi' => $this->renderer->render($serverapiIndex)]),
      'status',
      TRUE
    );
    return $this->entityTypeManager->getStorage('search_api_index')->create($default)->save();
  }

  /**
   * {@inheritDoc}
   */
  private function addFieldsIndex(array $fields, $datasource_id) {
    $field_settings = [];
    foreach ($fields as $fieldname => $field) {
      $type = 'string';
      switch ($field['type']) {
        case 'entity_reference':
        case 'list_integer':
        case 'integer':
          $type = 'integer';
          break;

        case 'decimal':
        case 'list_float':
          $type = 'decimal';
          break;

        case 'boolean':
          $type = 'boolean';
          break;

        case 'timestamp':
        case 'datetime':
        case 'daterange':
          $type = 'date';
          break;
      }
      $field_settings[$fieldname] = [
        'label' => $field['label'],
        'datasource_id' => $datasource_id,
        'property_path' => $fieldname,
        'type' => $type,
      ];
    }
    return $field_settings;
  }

  /**
   * Get import link.
   *
   * {@inheritDoc}
   */
  public function getImportLink($type = NULL, $bundle = NULL, $field = NULL, $entity_id = NULL) {
    $url = NULL;
    if (empty($type)) {
      return $url;
    }
    switch ($type) {
      case 'user':
        $url = Url::fromRoute('quick_data.content.import', [
          'type' => 'user',
          'bundle' => 'user',
        ]);
        break;

      case 'paragraph':
        if (empty($entity_id)) {
          $url = Url::fromRoute('help.page', [
            'name' => 'quick_data',
          ]);
        }
        else {
          $url = Url::fromRoute('quick_data.paragraph.import', [
            'type' => $type,
            'bundle' => $bundle,
            'field' => $field,
            'entity_id' => $entity_id,
          ]);
        }
        break;

      default:
        if (empty($bundle)) {
          return NULL;
        }
        $url = Url::fromRoute('quick_data.content.import', [
          'type' => $type,
          'bundle' => $bundle,
        ]);
        break;
    }
    return $url;
  }

}
