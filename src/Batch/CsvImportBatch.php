<?php

namespace Drupal\quick_data\Batch;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\File\FileSystemInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_store\Entity\Store;
use Drupal\field_collection\Entity\FieldCollection;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;

/**
 * Methods for running the CSV import in a batch.
 *
 * @package Drupal\csvimport
 */
class CsvImportBatch {

  /**
   * Handle batch completion.
   *
   *   Creates a new CSV file containing all failed rows if any.
   */
  public static function csvImportFinished($success, $results, $operations) {

    $messenger = \Drupal::messenger();
    $fileSystem = \Drupal::service('file_system');
    if (!empty($results['failed_rows'])) {

      $dir = 'public://csvimport';
      if ($fileSystem->prepareDirectory($dir, FileSystemInterface::CREATE_DIRECTORY)) {

        // We validated extension on upload.
        $csv_filename = 'failed_rows-' . basename($results['uploaded_filename']);
        $csv_filepath = $dir . '/' . $csv_filename;

        $targs = [
          ':csv_url'      => \Drupal::service('file_url_generator')->generateAbsoluteString($csv_filepath),
          '@csv_filename' => $csv_filename,
          '@csv_filepath' => $csv_filepath,
        ];

        if ($handle = fopen($csv_filepath, 'w+')) {

          foreach ($results['failed_rows'] as $failed_row) {
            fputcsv($handle, $failed_row);
          }

          fclose($handle);
          $messenger->addMessage(t('Some rows failed to import. You may download a CSV of these rows: <a href=":csv_url">@csv_filename</a>', $targs), 'error');
        }
        else {
          $messenger->addMessage(t('Some rows failed to import, but unable to write error CSV to @csv_filepath', $targs), 'error');
        }
      }
      else {
        $messenger->addMessage(t('Some rows failed to import, but unable to create directory for error CSV at @csv_directory', $dir), 'error');
      }
    }

    return t('The CSV import has completed.');
  }

  /**
   * Process a single line.
   */
  public static function csvImportLine($line, &$context, $mapping = []) {
    self::quickDataImportMapping($context, $mapping, $line);
    if (!is_array($context)) {
      return;
    }
    if (!isset($context['results'])) {
      $context['results'] = [];
    }
    if (!isset($context['results']['rows_imported'])) {
      $context['results']['rows_imported'] = 0;
    }
    $context['results']['rows_imported']++;
    $context['message'] = t('Importing row !c', ['!c' => $context['results']['rows_imported']]);
  }

  /**
   * {@inheritDoc}
   */
  public static function quickDataImportMapping($store, $mapping, $data = []) {
    $id = $store['entity_id'];
    $host = FALSE;
    $num_imported = 0;
    $entityTypeManager = \Drupal::entityTypeManager();
    $currentUser = \Drupal::currentUser();
    $messenger = \Drupal::messenger();
    if (in_array($store['type'], ['field_collection_item', 'paragraph']) && $id) {
      switch ($store['bundle']) {
        case 'node':
          $host = $entityTypeManager->getStorage('node')->load($id);
          break;

        case 'user':
          $host = $entityTypeManager->getStorage('user')->load($id);
          break;

        case 'field_collection_item':
          // @phpstan-ignore-next-line
          $host = FieldCollection::load($id);
          break;

        case 'paragraph':
          // @phpstan-ignore-next-line
          $host = Paragraph::load($id);
          break;

        case 'taxonomy_term':
          $host = $entityTypeManager->getStorage('taxonomy_term')->load($id);
          break;

        default:
          $host = $entityTypeManager->getStorage($store['bundle'])->load($id);
          break;
      }
    }

    foreach ($data as $row) {
      $table_field = [];
      $field_values = [];
      foreach ($row as $col => $value) {
        if (empty($value) || $mapping[$col] == '_none') {
          continue;
        }
        $field = $mapping[$col];
        $checkTarget = explode(':', $field);
        $target = FALSE;
        if (!empty($checkTarget[1])) {
          $field = $checkTarget[0];
          $target = $checkTarget[1];
        }
        $table_field[$field] = $value;
        self::quickDataPrepareData($field_values, $store['fields_info'][$field], $value, $target);
      }
      if (empty($field_values)) {
        return FALSE;
      }
      switch ($store['type']) {
        case 'node':
          $default = [
            'type' => $store['bundle'],
            'uid' => $currentUser->id(),
            'status' => 1,
          ];
          $fieldsNode = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $store['bundle']);
          if (isset($fieldsNode['comment'])) {
            $default['comment_status'] = 0;
            $field_values['comment'] = NULL;
          }
          $is_new = TRUE;
          if (!empty($store['field_unique'])) {
            $entities = \Drupal::entityTypeManager()
              ->getStorage('node')
              ->loadByProperties([
                'type' => $store['bundle'],
                $store['field_unique'] => $table_field[$store['field_unique']],
              ]);
            if (!empty($entities)) {
              $entity = current($entities);
              $is_new = FALSE;
            }
          }
          if ($is_new) {
            $entity = Node::create(array_merge($default, $field_values));
          }
          try {
            $entity->enforceIsNew();
            $entity->save();
          }
          catch (\Exception $e) {
            $messenger->addError($e->getMessage());
          }
          break;

        case 'user':
          $entity = User::create($field_values);
          $violations = $entity->validate();
          if (empty($violations->count())) {
            $entity->enforceIsNew();
            $entity->activate();
            $entity->setPassword(\Drupal::service('password_generator')->generate(6));
            $entity->save();
          }
          else {
            foreach ($violations as $violation) {
              $messenger->addError($violation->getMessage());
            }
          }
          break;

        case 'taxonomy_term':
          $default = ['vid' => $store['bundle']];
          $entity = Term::create(array_merge($default, $field_values));
          $entity->save();
          break;

        case 'paragraph':
          if ($host) {
            $content_type = $host->getType();
            $field_instance = $entityTypeManager->getStorage('field_config')
              ->load(implode('.', [
                $host->getEntityTypeId(),
                $content_type,
                $store['field'],
              ]));
            $paragraph_type = array_key_first($field_instance->getSetting("handler_settings")["target_bundles"]);
            $default = [
              'type' => $paragraph_type,
              'parent_id' => $host->id(),
              'parent_type' => $content_type,
              'parent_field_name' => $store['field'],
            ];
            // @phpstan-ignore-next-line
            $paragraph = Paragraph::create(array_merge($default, $field_values));
            $paragraph->isNew();
            $paragraph->save();
            $entity_items[] = [
              'target_id' => $paragraph->id(),
              'target_revision_id' => $paragraph->getRevisionId(),
            ];
          }
          break;

        case 'field_collection_item':
          if ($host) {
            // @todo if module field collection is release.
            $content_type = $host->getType();
            $field_instance = $entityTypeManager->getStorage('field_config')
              ->load(implode('.', [
                $host->getEntityTypeId(),
                $content_type,
                $store['field'],
              ]));

          }
          break;

        case 'data':
          if (!empty($table_field)) {
            \Drupal::database()->insert($store['bundle'])->fields($table_field)->execute();
          }
          break;

        case 'commerce_product':
          $default = [
            'type' => $store["bundle"],
          ];
          // @phpstan-ignore-next-line
          $product = Product::create(array_merge($default, $field_values));
          $product->save();
          break;

        case 'commerce_store':
          $default = [
            'type' => $store["bundle"],
            'uid' => $currentUser->id(),
          ];
          // @phpstan-ignore-next-line
          $commerce_store = Store::create(array_merge($default, $field_values));
          $commerce_store->save();
          break;

        default:
          $entity = $entityTypeManager->getStorage($store['type'])->create($field_values);
          $entity->save();
          break;
      }
      $num_imported++;
    }
    if (!empty($entity_items)) {
      foreach ($entity_items as $entity_item) {
        $host->get($store['field'])->appendItem($entity_item);
      }
      $host->save();
    }
    return $num_imported;
  }

  /**
   * {@inheritDoc}
   */
  public static function quickDataPrepareData(&$field_values, $field_info, $value, $target = FALSE) {
    $requestStack = \Drupal::service('request_stack')->getCurrentRequest();
    $file_repository = \Drupal::service('file.repository');
    $entityTypeManager = \Drupal::entityTypeManager();
    $fileSystem = \Drupal::service('file_system');
    $languageManager = \Drupal::languageManager();
    $currentUser = \Drupal::currentUser();
    $token = \Drupal::token();
    $field = $field_info['field_name'];
    $timeFormat = 'Y-m-d H:i:s';
    if (!empty($field_info["columns"]) && !$target) {
      $target = array_key_first($field_info["columns"]);
    }
    if ($field_info['cardinality'] == -1 || $field_info['cardinality'] > 1) {
      $separate = ',';
      if ($field_info["widget"] == 'text_textfield') {
        $separate = '|';
      }
      if (!empty($field_info["schema"]["value"]["type"]) && $field_info["schema"]["value"]["type"] == 'text') {
        $separate = '|';
      }
      $values = explode($separate . ' ', $value);
    }
    else {
      $values = [$value];
    }
    $language = $languageManager->getCurrentLanguage()->getId();
    $host = $requestStack->getHost();
    $public = 'public://';
    foreach ($values as $index => $value) {
      switch ($field_info['type']) {
        case 'user_reference':
          if (!is_numeric($value)) {
            $user_load = user_load_by_name($value);
            if ($user_load) {
              $value = $user_load->uid;
            }
          }
          break;

        case 'node_reference':
        case 'entity_reference':
          if (!is_numeric($value)) {
            $info = ['title' => trim($value), 'langcode' => $language];
            if (in_array($field_info["settings"]["target_type"], [
              'taxonomy_term',
              'user',
            ])) {
              $info = ['name' => $value, 'langcode' => $language];
            }
            $entities = $entityTypeManager->getStorage($field_info["settings"]["target_type"])->loadByProperties($info);
            if (!empty($entities)) {
              foreach ($entities as $entity) {
                $entityType = FALSE;
                if ($field_info["settings"]["target_type"] == 'taxonomy_term') {
                  $entityType = $entity->bundle();
                }
                elseif ($field_info["settings"]["target_type"] == 'user') {
                  $value = $entity->id();
                }
                else {
                  $entityType = $entity->getType();
                }
                if ($entityType && !empty($field_info["settings"]["handler_settings"]["target_bundles"][$entityType])) {
                  $value = $entity->id();
                  break;
                }
              }
            }
            else {
              if ($field_info["settings"]["target_type"] == 'user') {
                $mail = $value;
                if (\Drupal::moduleHandler()->moduleExists('pathauto')) {
                  $mail = \Drupal::service('pathauto.alias_cleaner')->cleanString($value);
                }

                $info = [
                  'name' => $value,
                  'pass' => $value,
                  'mail' => str_replace([' ', '-'], '.', strtolower($mail)) . '@' . $host,
                  'status' => TRUE,
                  'langcode' => $language,
                ];
              }
              elseif ($field_info["settings"]["target_type"] == 'taxonomy_term') {
                $info = [
                  'name' => $value,
                  'vid' => array_key_first($field_info["settings"]["handler_settings"]["target_bundles"]),
                  'langcode' => $language,
                ];
              }
              elseif (!empty($field_info["settings"]["handler_settings"]["target_bundles"])) {
                $info['type'] = array_key_first($field_info["settings"]["handler_settings"]["target_bundles"]);
              }
              $entity = $entityTypeManager->getStorage($field_info["settings"]["target_type"])->create($info);
              $entity?->enforceIsNew();
              $entity->save();
              $value = $entity->id();
            }
          }
          break;

        case 'list_integer':
        case 'list_float':
        case 'list_boolean':
        case 'list_text':
        case 'list_string':
        case 'list_states':
          if (!empty($field_info["allowed_values"]) && empty($field_info["allowed_values"][$value])) {
            $flip_allowed_values = array_flip($field_info["allowed_values"]);
            $value = !empty($flip_allowed_values[$value]) ? $flip_allowed_values[$value] : NULL;
          }
          break;

        case 'number_integer':
        case 'number_float':
        case 'number_decimal':
        case 'integer':
        case 'float':
        case 'decimal':
          $value = str_replace([',', ''], ['.', ''], $value);
          if (!is_numeric($value)) {
            $value = NULL;
          }
          break;

        case 'file':
        case 'image':
          if (!is_int($value)) {
            $arr = parse_url($value);
            $paths = explode('/', $arr['path']);
            $filename = end($paths);
            // Check if file exits or url.
            if (UrlHelper::isValid($value, TRUE) || (file_exists($value) && empty($arr['scheme']))) {
              if (!empty($arr)) {
                $data = file_get_contents($value);
                $folder = '';
                if (!empty($field_info["settings"]["file_directory"])) {
                  $field_info["settings"]["file_directory"] = $token->replace($field_info["settings"]["file_directory"]);
                  $directory = $public . $field_info["settings"]["file_directory"];
                  $fileSystem->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY);
                  $folder = $field_info["settings"]["file_directory"];
                }
                $filePublic = $public . $folder . DIRECTORY_SEPARATOR . $filename;
                $file = $file_repository->writeData($data, $filePublic);
                $value = $file->id();
              }
            }
            else {
              $files = $entityTypeManager->getStorage('file')->loadByProperties(['uri' => $value]);
              if (!empty($files)) {
                $value = array_keys($files)[0];
              }
              else {
                $filepath = $fileSystem->realpath($value);
                $file = File::create([
                  'uri' => $value,
                  'uid' => $currentUser->id(),
                  'status' => FileInterface::STATUS_PERMANENT,
                  'filemime' => mime_content_type($filepath),
                  'filename' => $filename,
                ]);
                $file->setPermanent();
                $file->save();
                $value = $file->id();
              }
            }
          }
          break;

        case 'daterange':
          [$start_value, $end_value] = array_pad(explode(' - ', $value), 2, NULL);
          $dateTimeFormat = 'Y-m-d\TH:i:s';
          // Only date.
          if (!empty($field_info["settings"]["datetime_type"]) && $field_info["settings"]["datetime_type"] == 'date') {
            $dateTimeFormat = 'Y-m-d';
          }
          if (!empty($start_value)) {
            $start_value = date($timeFormat, strtotime(str_replace('/', '-', $start_value)));
            $dateTime = \DateTime::createFromFormat($timeFormat, $start_value);
            $start_value = $dateTime->format($dateTimeFormat);
          }
          if (!empty($end_value)) {
            $end_value = date($timeFormat, strtotime(str_replace('/', '-', $end_value)));
            $dateTime = \DateTime::createFromFormat($timeFormat, $end_value);
            $end_value = $dateTime->format($dateTimeFormat);
          }
          else {
            $end_value = $start_value;
          }
          $value = [
            'value' => $start_value,
            'end_value' => $end_value,
          ];
          break;

        case 'date':
        case 'created':
        case 'changed':
        case 'datetime':
        case 'datestamp':
          $timeConvert = date($timeFormat, strtotime(str_replace('/', '-', $value)));
          $dateTime = \DateTime::createFromFormat($timeFormat, $timeConvert);
          $dateTimeFormat = 'Y-m-d\TH:i:s';
          // Only date.
          if (!empty($field_info["settings"]["datetime_type"]) && $field_info["settings"]["datetime_type"] == 'date') {
            $dateTimeFormat = 'Y-m-d';
          }
          $value = $dateTime->format($dateTimeFormat);
          if (($field_info["schema"]["value"]["type"] ?? NULL) === 'int') {
            $value = $dateTime->getTimestamp();
          }
          break;

        case 'geofield':
          $coord = explode(',', $value);
          $value = \Drupal::service('geofield.wkt_generator')->WktBuildPoint($coord);
          break;

        case 'commerce_price':
          $price = filter_var($value, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
          $price = str_replace(',', '.', $price);
          $currencies = $entityTypeManager->getStorage('commerce_currency')->loadMultiple();
          $currency_code = array_key_first($currencies);
          // @phpstan-ignore-next-line
          $value = new Price($price, $currency_code);
          break;

        case 'text_with_summary':
          $value = ['value' => $value, 'format' => 'full_html'];
          break;
      }
      if (!is_null($value)) {
        if (!$target) {
          $target = 'value';
        }
        if (is_array($value)) {
          $field_values[$field][$index] = $value;
        }
        else {
          if (!empty($field_info['schema'][$target]['length'])) {
            $value = mb_substr($value, 0, $field_info['schema'][$target]['length']);
          }
          $field_values[$field][$index][$target] = $value;
        }
      }
    }
  }

}
