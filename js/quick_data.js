/**
 * @file
 * Attaches the behaviors for the SN QUICK Add field data module.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';
  Drupal.behaviors.quick_data = {
    attach: function (context, settings) {
      if (settings.quick_data.entity_type != "data") {
        $(".widget-type-select option:selected").parent().siblings().hide();
        $(once('quick-data', ".field-type-select", context)).on('change', function () {
          let type_label = $(this).val();
          $(this).closest("tr").find(".widget-type-select").
          find("optgroup, option").hide().
          filter("[label='" + type_label + "'], [label='" + type_label + "'] > *").show().
          find("option:first").attr("selected", "selected");
        });
      }
      $(once('quick-data_admin-link', ".admin-link a", context)).click(function (event) {
        event.preventDefault();
        $(this).closest("td").find("input").prop("type", "text");
        $(this).closest("small").remove();
      });
    }
  };

  Drupal.behaviors.quick_data_import = {
    attach: function (context, settings) {
      $(once('quick-data_export', "#export", context)).click(function (event) {
        let separator = $('#edit-csv-delimiter').val();
        if(separator == 'tab'){
          separator = "\t";
        }
        let outputCSV = $(this).parent().find('#export-header').text();
        let columns = outputCSV.split(",");
        if(columns.length == 1){
          columns = outputCSV.split(";");
          if(columns.length == 1){
            columns = outputCSV.split("\t");
          }
        }
        outputCSV = columns.join(separator);
        const universalBOM = "\uFEFF";
        let blobby = 'data:application/csv;charset=utf-8,' + encodeURIComponent(universalBOM + outputCSV);
        $(this).attr({
          'href': blobby,
          'target': '_blank'
        });
      });
    }
  };
})(jQuery, Drupal);
