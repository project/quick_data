# Quick data addition field

This module helps you quickly create / add fields to content type.

## Scenario

You have Excel data with 20 columns, you want to show it quickly.
you must create field by field for each column, after that create views/
index field and for each field, define field by field in the feeds for import.
It takes a lot of time if you have a lot of columns
Quick data helps you create / add all fields in 1 minute.
You just need to copy the Excel table header to "field label" and select
data type, it will automatically create / add content type views and feeds

## Import
if you don't want to use feed to import csv, you can import your data
with quick data, it will match column name with field label

https://yoursite.com/quick_data/import/{entity_type}/{content_type}


example: Content type - Basic page
https://yoursite.com/quick_data/import/node/page


for paragraph field, you can import paragraph field this way
https://yoursite.com/quick_data/import/paragraph/{entity_type}/{field_name}/{entity_id}

example: import data to the field paragraphs table in node/1 - Basic page
https://yoursite.com/quick_data/import/paragraph/node/para_table/1
